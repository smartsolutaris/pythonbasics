import os
"""
File Handling
- create
- read
- update
- delete

Open() function
- parameters - filename and the mode
open(<<filename:str>>, <<mode:str>>)
- r => read, opens a file for reading, error if file doesnt exist.
- a => append, open a file for appending, creates a file if it does not exist
- w => write,  open a file for writing, creates a file if does not exist.
    It overwrites the file
- x => create, create a file, error if file exists

- t => text mode (default)
- b => binary mode

methods:
- read(): returns whole content of a file by default
- read(<<number>>): returns the number of characters specified
- readline()
- write() : writes to a file

To delete a file, the module os must be imported
os.remove: returns error if file not found
"""
f_write = open("text1.txt", "w")
f_write.write("It's a wonderful day")
f_write.close()

f_append = open("text1.txt", "a")
f_append.write("\nThe sun is shining and the weather is cool")
f_append.close()

with open("text1.txt", "a") as f:
    f.write("\nThe weather is great for riding")

f = open("text1.txt", "r")
# for lines in f:
#     print(lines.rstrip('\n'))
while True:
    line = f.readline()
    if not line:
        break
    print(line.rstrip('\n'))
f.close()
# f_excel = open("text.csv", "r")
# print(f_excel.read())
# f_excel.close()

if os.path.exists("text.xlsx"):
    os.remove("text.xlsx")



