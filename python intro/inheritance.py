from rectangle import Rectangle
"""
    inheritance allows a child class to inherit base class methods and attributes
class Baseclass:
    pass

class Childclass(Baseclass):
    pass
"""

class Parallelogram(Rectangle):
    # def __init__(self, length, breadth, height):
    #     Rectangle.__init__(self,length, breadth)
    #     self.height = height

    def __init__(self, length, breadth, height):
        super().__init__(length, breadth)
        self.height = height

    def volume(self):
        return  self.length * self.breadth * self.height



parl = Parallelogram(10, 3, 5)
parl.display()
print(parl.area())
print(parl.volume())
