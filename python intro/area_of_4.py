#Type hinting
"""
    def <<function_name>>(param:<<type>>) -> <<type>>:
        statement
"""
# Area of 4 sided shapes
# square = l*l
# rectangle = L*b


def area_of_square(len):
    return len ** 2


def area_of_rectangle(len, br):
    return len * br


def area_of_quadrilateral(length:float = 10, breadth:float = 10) -> float:
    if length == breadth:
        return area_of_square(length)
    else:
        return area_of_rectangle(length, breadth)


print(area_of_quadrilateral(5, 7))
print(area_of_quadrilateral())