# List Exercise

* Reverse a given list in python
```python
 info = ['karl', '100', 'Red', 'Mangoes']
```
* Write a program to add two lists index-wise. Create a new list that contains the 0th index item from both the list, then the 1st index item, and so on till the last element. any leftover items will get added at the end of the new list.
`Hint: use list comprehension with zip function`
```python
list1 = ["M", "na", "i", "Ke"]
list2 = ["y", "me", "s", "lly"]

result = ['My', 'name', 'is', 'Kelly']
```
* Write a Python program to find the second largest number in the given list.
```python
Input: list1 = [10, 20, 4]
result: 10

Input: list2 = [70, 11, 20, 4, 100]
result: 70
```
* Concatenate two list
`Hint: use list comprehension`
`<<new_list>> = [expression for item in list1 for y in list2]`
```python
list1 = ["Hello ", "take "]
list2 = ["Dear", "Sir"]

result = ['Hello Dear', 'Hello Sir', 'take Dear', 'take Sir']
```
* Write a program to find value 20 in the list, and if it is present, replace it with 200. Only update the first occurrence of an item.
```python
list1 = [5, 10, 15, 20, 25, 50, 20]
```
* count number of occurrences of x in the given list
```python
Input : lst = [15, 6, 7, 10, 12, 20, 10, 28, 10]
         x = 10
result : 3  #10 appears three times in given list.

Input : lst = [8, 6, 8, 10, 8, 20, 10, 8, 8]
        x = 16
result : 0
```
* write a program to remove all occurrences of item 20
`Hint: list comprehension`
```python
list1 = [5, 20, 15, 20, 25, 50, 20]
```

* Write a program to return the middle value of a list. If there are 2 middle values, return the second
```python
Example: names = ['Ade', 'orange', 'pineapple', 'apples', 'mangoes']
result = 'pineapple'

age = [10, 3, 45, 67, 89.0, 45]
result = 67
```