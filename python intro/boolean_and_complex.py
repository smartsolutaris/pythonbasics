#looping through a list

## 1. Using For loop
fruits = ['orange', 'pineapple', 'apples', 'mangoes']
# keywords are special words reserved for python
for val in fruits:
    print(f"I love {val}")


numbers = [2, 5, 7, 8, 9]
numbers_cube = []
for num in numbers:
    numbers_cube.append(num ** 3)
    #print(numbers_cube)
    #print(num ** 3)

print(numbers_cube)


# List comprehension
# shorten the syntax for iterating a list
# creates a new list

# <<new_list>> = [expression for item in iterable<<list>> if condition == True]
new_cube_root = [num ** 3 for num in numbers]
print(new_cube_root)

new_cube_val = [num**3 for num in numbers if num % 2 == 0]
print(new_cube_val)

names = ['Ariel', 'Paul', 'Thomas']
names_upper = [x.upper() for x in names]
print(names_upper)