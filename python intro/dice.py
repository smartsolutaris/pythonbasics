"""
Create a class for a dice game:
    = dice has 6 side [1,2,3,4,5,6]
        = You can use multiple dice 3,4
    = method roll dice
    = method that computes the sum of dice:
        if 6/66/666 is rolled, opprotunity to roll again

"""
from random import choice

class Dice:
    def __init__(self):
        self.__faces = [1,2,3,4,5,6]

    def roll_a_dice(self):
        return choice(self.__faces)

class MyDiceGame(Dice):
    def __init__(self, no_dice:int = 1):
        super().__init__()
        self.__check_dice_number(no_dice)
        self.no_dice = no_dice

    def __check_dice_number(self, no_dice):
        if type(no_dice) is not int:
            return "Error"

    def roll_dice(self):
        val = []
        for n in range(self.no_dice):
            rolled = self.roll_a_dice()
            val.append(rolled)
            if rolled == 6:
                val.append(self.roll_a_dice())
        return val, sum(val)


m = MyDiceGame(1)
print(m.roll_dice())