"""
Working with a deck of cards
52

"""
from random import shuffle, choice

class Card:
    def __init__(self):
        self.suites = ['Hearts', 'Clubs', 'Spade', 'Diamonds']
        self.values = ['A', '2', '3', '4', '5','6','7','8','9','10','J','K','Q']
        self.__deckset = []
        self.__deck()

    def __deck(self):
        """52 cards in a deck"""
        for c in self.suites:
            for n in self.values:
                self.__deckset.append(f"{n} of {c}")

    @property
    def deck_of_card(self):
        return self.__deckset

    def shuffle(self):
        """
            shuffle the deck of cards
        :return:
            deckset: List
        """
        if len(self.__deckset) < 52:
            return "Cannot shuffle"
        shuffle(self.__deckset)
        return self.__deckset

    def popCard(self):
        if len(self.__deckset) == 0:
            return "Empty deck"
        popped = self.__deckset.pop()
        return popped


class MyCardGame(Card):
    def __init__(self):
        super().__init__()
        self.card_set = self.shuffle()
        self.__pot = []

    def assign_players_card(self, no_players:int = 2, no_cards:int = 4):
        if no_players <= 0 or no_cards <= 0:
            return None
        if len(self.card_set) / (no_players * no_cards) < 0:
            return "Insufficient Cards to play"

        player_cards = [[] for i in range(no_players)]
        for i in range(no_players):
            for val in range(no_cards):
                player_cards[i].append(self.card_set.pop())

        return player_cards

    def play_a_card(self, cards):
        if len(cards) > 0:
            popped = choice(cards)
            cards.remove(popped)
            self.__pot.append(popped)
            return popped
        return None

    @property
    def pot(self):
        return self.__pot

m = MyCardGame()
players = m.assign_players_card(3,4)
print(players)
player1 = players[0]

print(m.play_a_card(player1))
print(m.play_a_card(players[1]))
print(m.play_a_card(players[2]))
print(m.pot)








