"""
    unordered, unchangeable and unindexed
    Duplicates are not allowed
    written with {}
"""
my_first_set = {"fred", "Tolu"}
print(type(my_first_set))
fruits = {"apples", "mangoes", "bananas", "apples", "grapes", "bananas"}
print(fruits)

# add items using add()
fruits.add("oranges")
fruits.add("cherry")
fruits.add("mangoes")

print(fruits)

# Add elements from another set -> update()
vegetables = {"tomatoes", "cucumber", "starfruit", "passion"}
fruits.update(vegetables)
print(fruits)

# add list to a set
roots = ["carrots", "beet"]
fruits.update(roots)
print(fruits)

#Accessing elements of a set
# all element use Loop => for loop
"""
    for <<variable_name_for_each_item>> in [iterable]:
        statements
"""
for fruit in fruits:
    # expressions/statements
    #pass
    print(f"I love {fruit}")

# if element exist => in
print("grapes" in fruits)
print("sugar cane" in fruits)

# remove item in a set => remove()
# error occurs when you remove an item that doesnt exist
fruits.remove("grapes")
print(fruits)

#fruits.remove("sugar")
#print(fruits)

#discard
# No error occurs when you remove an item that doesnt exist
fruits.discard("apples")
fruits.discard("sugar")

# pop removes the last element in a set
print(fruits)
popped_fruit = fruits.pop()
print(popped_fruit)
print(fruits.pop())

# intersection = what is common in 2 sets. what exists in set1 and set2
names1 = {"andy", "carroll", "april"}
names2 = {"april", "kareem", "Ben"}
print(names1.intersection(names2))

# symmetric difference => returns not common elements in both sets
# (names1 + names2) - intersection(names1, names2)
# {"andy", "carroll", "april","kareem", "Ben"} - {"april"}
print(names1.symmetric_difference(names2))

# difference returns the set of difference btw sets
# names1 - intersection(names1, names2)
# {"andy", "carroll", "april"} - {"april}
print(names1.difference(names2))

# Disjoint returns True if 2 sets dont have an intersection
print(names1.isdisjoint(names2))
print(names1.isdisjoint(fruits))

print(len(fruits))

# Check if 2 list have at least one common element
list1 = [2,4, 5, 3, 2, 4,5]
list2 = [3,5,6,0,8,9]

list1 = set(list1)
list2 = set(list2)

print(f"My list dont have common ground: {list1.isdisjoint(list2)}")
