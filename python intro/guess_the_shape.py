"""

Guess the shape

3 sided = triangle
4 sided = l == b square
        l==l, b == b  rectangle
        l != l b!B = parallelogram
"""

class GuessShape:
    def __init__(self, no_of_sides, length = 0, breadth = 0):
        self.no_of_sides = no_of_sides
        self.length = length
        self.breadth = breadth

    def guess_shape(self, ):
        if self.no_of_sides == 1:
            return "line"
        elif self.no_of_sides == 2:
            return "L"
        elif self.no_of_sides == 3:
            return "triangle"
        elif self.no_of_sides == 4:
            return self.four_sided_shape()
        else:
            return None

    def four_sided_shape(self):
        if self.length == self.breadth:
            return "Square"
        return "Rectangle"

m = GuessShape(2, 3, 5)
print(m.guess_shape())
