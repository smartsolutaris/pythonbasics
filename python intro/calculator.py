from math import sqrt
"""
Create a class Calculator that should perform the following actions:
Addition / Subtraction
Multiplication / Division
Take sqrt root of number
Reset memory (Calculator must have its own memory, meaning it should manipulate its
starting number 0 until it is reset.).
This means that for example calculator should perform actions with a value
inside its memory (for this example value inside the calculator's memory is 0):
calculator.add(2) results in 2.
"""

class Calculator:
    def __init__(self):
        self.__memory = 0

    def add(self, number):
        self.__memory += number # self.__memory = self.__memory + number
        return self.__memory

    def sub(self, number):
        self.__memory -= number
        return self.__memory

    def multi(self, number):
        self.__memory *= number
        return  self.__memory

    def div(self, number):
        self.__memory /= number
        return self.__memory

    def sqroot(self):
        self.__memory = sqrt(self.__memory)
        return self.__memory

    def reset(self):
        self.__memory = 0

    @property
    def memory(self):
        return self.__memory

calc = Calculator()
print(calc.add(10))
print(calc.add(8))
print(calc.sub(7))
print(calc.multi(3))
print(calc.div(2))
print(calc.sqroot())
print(calc.add(2))
calc.reset()
print(calc.memory)
print(calc.add(2))
print(calc.memory)

