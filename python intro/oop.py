"""
    Object Oriented Programming
=> objects
- state/attributes => variables
- behaviour/methods => functions

AN object is an instance of a class
A class is a blueprint/template for creating objects
    - specify attributes (default values)
    - specify the behaviour

Adv:
- bundle data and functionality together
- creating an object, allows new instance of the type to be made
- has methods for modifying its state
- create a user-defined data structure

- keyword class



Car = class
- minimum of 2 doors
- maximum of 4 doors
- 4 wheels
- windows/windshields
- lights
- maximum length
- maximum seating number - 5

object = Nissan, Volvo, Benz, BMW, Hyundai, Toyota

Tricycle = class
- sometimes have doors
- 3 wheels

bikes = class
- 2 wheels

unicyles = class
- 1 wheel

Dogs:
- breed
- age
- fur color

malamutes, shephard, corgi,

attributes are always public except specified and accessed using dot(.) e.g Car.windows

constructor: special method

class Car:
    def __init__(self):
        pass

    def methods(self):
        pass
"""


class Car:
    """
        Class Car:
        variables:
            Name - Name of the car
            doors - Number of doors the car has
        methods:
            print_details - prints information about the car
    """
    NAME = None # class variable (optional)
    doors = 0 # class variable (optional)

    def __init__(self, name_of_car:str, no_of_doors:int):
        self.NAME = name_of_car
        self.doors = no_of_doors

    def print_details(self):
        print(f"Car name: {self.NAME}, number of doors:{self.doors}")

# object definition
# <<objname>> = <<classname(optional parameters)>>


nissan = Car("Nissan Passat 2020", 4) # object of class Car
volvo = Car("Volvo S90", 4)

print(nissan.doors)
print(nissan.NAME)
nissan.print_details()

nissan.NAME = "Nissan V23"
nissan.doors = 2
nissan.print_details()

volvo.print_details()

print(type(nissan))


class Dog:
    """
        Class Dog:
            variables - name, breed, age
            methods - 4 methods

    """
    def __init__(self):
        self.name = "Jonny"
        self.breed = "malamute"
        self.age = 10 #years
        self.fur_color = "brown"

    def print_details(self) -> str:
        print(f"I am {self.name} a {self.breed} and {self.age} years old, fur color is {self.fur_color}")

    def compute_dog_years(self) -> float:
        return self.age * 2

    def verify_best_weather(self) -> str:
        if self.breed.lower() in ["malamute", "chow", "husky", "mountain lion"]:
            return "Cold"
        else:
            return "warm"

    def vaccination_status(self, vaccine_status:bool = False) -> str:
        if vaccine_status:
            print(f"{self.name} is vaccinated")
        else:
            print(f"{self.name} is not vaccinated")


dog1 = Dog()
dog1.name = "Rex"
dog1.age = 2

dog1.print_details()
dog1.vaccination_status(True)

dog1.vaccination_status()
print(dog1.verify_best_weather())