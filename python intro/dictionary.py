from collections import Counter

# dictionaries are stored in key:value pair
"""
    key: value
    ordered, changeable, does not allow duplicate
"""
list1 = [1,2,3,4]
set1 = {2, 3,5}
tuple1 = (2,3,4)
my_dict = {
    "name": "oyindolapo",
    "age": "28",
    "car": "benz"
}

phone_book = {
    "John": 87828882,
    "Doe": 783645487,
    "Jane": 46365745,
    "John": 23543544
}

print(type(phone_book))
# accessing items of a dict
print(phone_book['John'])

print(phone_book.get("Doe"))
# the second argument is returned if key does not exist
print(phone_book.get("Does", "No value"))

# All keys in dict => .keys()
print(phone_book.keys())

# All values in dict => .values()
print(phone_book.values())

# get each item in a dict => .items()
print(phone_book.items())

# change/add values
phone_book["Jane"] = 7777777
phone_book["april"] = 688888
print(phone_book)

# update, if key does not exist it adds it
phone_book.update({"Jane": 6277666})
phone_book.update({"april": 64364644})

# check if a key exist => in
print("John" in phone_book)

# removing items

#pop()

phone_book.pop("John")

# popitem() => removes the last inserted item
phone_book.popitem()

# looping For
## print key and values
for x in phone_book:
    print(f"{x} number is {phone_book[x]}")

for key, val in phone_book.items():
    print(f"{key} number is {val}")

for x in phone_book.values():
    print(x)

for x in phone_book.keys():
    print(x)

#print(phone_book)

# remove all duplicate word in a string
word = "Python is a great language C# is also a great language"
word_split = word.split(" ")
#set
set_word = set(word_split)
new_word = ' '.join(set_word)
print(new_word)

# dict
new_dict = dict.fromkeys(word_split)
new_word = ' '.join(new_dict)
print(new_word)

# collection => Counter
c = Counter(word_split)
new_word = ' '.join(c.keys())
print(new_word)

