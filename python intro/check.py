dividend=int(input("Please input dividend: "))
divisor=int(input("Please input divisor: "))
quotient=dividend//divisor # using float division

if quotient<0:
    print(f"The quotient equals {quotient+1}") # e.g. 7//-3=-3, but we need to have -2, therefore we add 1
if divisor < 0:
    pass
elif quotient > 2**31 - 1:  # According to the task the quotient cannot be greater than 2**31-1 which equals 2147483647
    print(f"The quotient equals {(2 ** 31) - 1}")
elif quotient > (-2**31): # According to the task the quotient cannot be less than -2**31 which equals -2147483648
    print(f"The quotient equals {min((-2**31))}")
else:
    print(f"The quotient equals {quotient}")