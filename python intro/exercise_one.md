## String Exercise
* Write a program to create a new string made of the middle three characters of an input string.
  ```python
    Example
    input = JhonDipPeta
    result = Dip
    ```
* Given two strings, s1 and s2. Write a program to create a new string s3 by appending s2 in the middle of s1.
```python
    Example: 
    s1 = Aunt
    s2 = Sister
    s3 = SisAuntter
```
* Given two strings, s1 and s2, write a program to return a new string made of s1 and s2’s first, middle, and last characters.
```python
    Example:
    s1 = America
    s2 = Japan
    s3 = AJrpan
```
* Write a program to check if two strings are balanced. For example, strings s1 and s2 are balanced if all the characters in the s1 are present in s2. The character’s position doesn’t matter.
```python
    Example:
    s1 = "Yn"
    s2 = "PYnative"
    result = True

    s1 = "Ynf"
    s2 = "PYnative"
    result = False
```
* Write a program to split a given string on hyphens and display first and last substring.
```python
    str1 = Emma-is-a-data-scientist
    result = Emma, scientist
```
* Reverse a given string
* Write a program to find the last position of a substring “Emma” in a given string.
```python
    str1 = "Emma is a data scientist who knows Python. Emma works at google."
    Result = Last occurrence of Emma starts at index 43
```
