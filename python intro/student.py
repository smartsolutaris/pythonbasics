"""
- create a student class that
    - accepts students information
    - display student information
    - search for a student
    - update student information
    - compute student grade

std = {
    std_name: {
        roll_no:roll_no,
        subject1: 20,
        subject2:30
    },
    std_name: {
        roll_no:roll_no,
        subject1: 20,
        subject2:30
    }
}

std = ["name", ]

Users always lie, You cannot trust the values from your user
- date of birth => calender, specify fields year[0-9]*4, !> year
"""

class Student:
    stdList = {}

    def __init__(self, name:str, std_no:str, sub1:int, sub2: int):
        self.__name = name
        self.__std_no = std_no
        self.__sub1 = sub1
        self.__sub2 = sub2

    def accept(self, std_name:str, std_no:str, sub1_score:int, sub2_score:int):
        if type(sub1_score) not in (int, float):
            print("Invalid score for subject 1")
            return
        if type(sub2_score) not in (int, float):
            print("Invalid score for subject 2")
            return
        std = {
            "name": std_name,
            "std_no":std_no,
            "sub1": sub1_score,
            "sub2": sub2_score
        }
        self.stdList.update({std_no: std})

    def all_students(self):
        for vals in self.stdList.values():
            print(f'{vals["name"]}: {vals["std_no"]}')

    def __compute_student_avg_score(self, roll_no:str):
        score = None
        if roll_no in self.stdList:
            score = (self.stdList[roll_no]["sub1"] + self.stdList[roll_no]["sub2"])/2
        return score

    def compute_grade(self, roll_no:str):
        score = self.__compute_student_avg_score(roll_no)
        if score is None:
            grade = 'N/A'
        elif score >= 95:
            grade = 'A+'
        elif (score >= 90) and (score < 95):
            grade = 'A'
        elif (score >= 80) and (score < 89):
            grade = 'A-'
        elif (score >= 70) and (score < 79):
            grade = 'B'
        elif (score >= 60) and (score < 69):
            grade = 'B-'
        elif (score >= 50) and (score < 99):
            grade = 'C'
        else:
            grade = 'F'
        return grade

    def search(self, roll_no):
        if roll_no in self.stdList:
            std = {
                "name": self.stdList[roll_no]["name"],
                "grade": self.compute_grade(roll_no),
                "score": self.__compute_student_avg_score(roll_no)
            }
            return std
        return "Student doesnt exist"

    def update_record(self, roll_no, name, sub1, sub2):
        # update () # updates a record if exist, add the record
        std = {
            "name": name,
            "std_no": roll_no,
            "sub1": sub1,
            "sub2": sub2
        }
        self.stdList.update({roll_no: std})


std1 = Student(None, None, 0, 0)
std1.accept("Shawn", "100001", 40, 56)
std1.accept("Shawn Boxer", "1000023", 50, 56)
std1.accept("Boxer", "10000231", "50", 56)
std1.all_students()
std1.update_record(name="Shawn", roll_no="100001", sub1=56 ,sub2= 75)

std1.accept("Shawn Dalot", "100001", 70, 75)

print(std1.search("100001"))
