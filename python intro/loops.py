# while and for

"""
while loop is used to execute statements while the condition remains valid
=> executes until condition is false

while <<condition>>:
    statements

break = stops the current execution and the loop
=> stops the loop even if the condition is valid

continue = stops the current execution and continues with the next one
"""
i = 0
while (i < 20):
    i += 1
    if i == 9:
        continue
    print(f"{i} : {i ** 2}")


for i in range(19):
    i += 1
    if i == 9:
        continue
    print(f"{i} : {i ** 2}")

