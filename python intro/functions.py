"""
    FUNCTIONS
    - block of code that runs when called
    - divide code into useful blocks
    - Dont repeat yourself
    - pass data into your function
    - !!! define a function before calling it

    # creating/defining a function
    def <<function_name>> ():
        statements

    #calling a function
    <<function_name>>()
"""


# greeting function
def greeting():
    print("Hello, welcome to class")

greeting()

# Arguments
# specified after the function
"""
    def <<function_name>> (param1, ......, paramn):
        statements
"""


def greet_by_name(firstname):
    print(f"Hello {firstname}, welcome to class")

greet_by_name("Oyin")
greet_by_name("Andy")


def sum_numbers(num1, num2, num3):
    print(f"num1 is {num1}")
    print(f"num2 is {num2}")
    print(f"num3 is {num3}")
    print(f"sum of 3 numbers is:{num1+num2+num3}")


sum_numbers(num1=2,num3=3, num2=5)
sum_numbers(2,3,5)
sum_numbers(2, 3, num3 = 5)
sum_numbers(2, num2=4, num3=5)

"""
Default parameters are values provided when defining functions
params are optional when calling your function
"""


def area_of_rectangle(length=5, breadth=10):
    print(f"length is {length}, breadth is {breadth} and area is {length * breadth}")

area_of_rectangle()
area_of_rectangle(10, 7)
area_of_rectangle(breadth=7)
area_of_rectangle(length=8)


#pir^2
def area_of_a_circle(radius, pi=3.12):
    print(f"area of circle is {pi * (radius ** 2)}")

area_of_a_circle(10)
area_of_a_circle(radius=10, pi=3)
#area_of_a_circle()

"""
Returning Values
- return
"""
val1 = area_of_a_circle(13)
print(val1)


def calculate_square(val = 1):
    return val ** 2

val_sqr = calculate_square(5)
print(val_sqr)


def determine_grade(score):
    if score >= 95:
        return 'A+'
    elif (score >= 90) and (score < 95):
        return 'A'
    elif (score >= 85) and (score < 89):
        return 'B+'
    else:
        return 'F'

user_score = 80
user_grade = determine_grade(user_score)
print(f"{user_score}:{user_grade}")


# args = arbituary arguments
# creating large number of positional argumnets, use args
def sum_n_numbers(*args):
    sum = 0
    for val in args:
        sum += val
    return sum


print(sum_n_numbers(12, 3, 4, 5))
print(sum_n_numbers(2,3))
print(sum_n_numbers(7,8,9,9,9,0,3,4,5,6))


# kwargs = arbituary keyword arguments
# **kwargs

def ingredent_for_omelette(**kwargs):
    for key, val in kwargs.items():
        print(f"added {val}: {key}")


ingredent_for_omelette(eggs = 4, carrots = 2, cheese = 3, onions = 1)
