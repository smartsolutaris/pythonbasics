"""
Write a Rectangle class in Python language, allowing you to build a rectangle with
length and width attributes.
Create a Perimeter() method to calculate the perimeter of the rectangle and a Area()
method to calculate the area of the rectangle.
Create a method display() that display the length, width, perimeter and area
of an object created using an instantiation on rectangle class.
"""


class Rectangle:
    def __init__(self, length: float = 10, breadth: float = 5):
        self.length = length
        self.breadth = breadth

    def perimeter(self):
        return self.length + self.breadth

    def area(self):
        return self.length * self.breadth

    def display(self):
        print(f"length: {self.length}, breadth: {self.breadth}, perimeter: {self.perimeter()}, "
              f"area: {self.area()}")


class Rectange2:
    def __init__(self, length: float = 10, breadth: float = 5):
        self.__length = length
        self.__breadth = breadth

    @property
    def length(self):
        return self.__length

    @length.setter
    def length(self, len):
        self.__length = len

    def perimeter(self):
        return self.__length + self.__breadth

    def area(self):
        return self.__length * self.__breadth

    def display(self):
        print(f"length: {self.__length}, breadth: {self.__breadth}, perimeter: {self.perimeter()}, "
              f"area: {self.area()}")


# rect = Rectange2(10, 8)
# rect.display()
#
# rect2 = Rectange2()
# rect2.length = 16
# rect2.display()



#
# rec = Rectangle(20, 5)
# rec.display()
#
# rec2 = Rectangle()
# rec2.length = 23
# rec2.breadth = 13
# rec2.display()
#
# rec2_dup = rec2 #referencing => created a link btw 2 two variables
# rec2_dup.display()
# rec2.display()
#
# rec2.breadth = 10
# print(rec2.breadth)
# print(rec2_dup.breadth)

