"""
    Privacy levels in a class
    - public (default)
    - protected  = only accessible by the class and child classes
    self._<<variablename>>
    - private = only accessible within the class
    self.__<<variablename>>
"""

class PythonStudent:
    def __init__(self, name:str = "Max", age:int = 20):
        self.__age = age
        self.__name = name

    def print_details(self):
        print(f"{self.__name} :{self.__age}")

    def get_name(self):
        return self.__name

    def set_name(self, name):
        self.__name = name

    def get_age(self):
        return  self.__age

    def set_age(self, age):
        if age > 0:
            self.__age = age


john = PythonStudent()
john.set_name("John")
john.set_age(23)
john.print_details()

"""
getter and setter

getter => return a value 
        - has @property decorator
setter => set a new valur for a variable
"""
class PythonStudent1:
    def __init__(self, name:str = "Max", age:int = 20):
        self.__age = age
        self.__name = name

    def print_details(self):
        print(f"{self.__name} :{self.__age}")

    @property
    def age(self):
        return self.__age

    @age.setter
    def age(self, age):
        if age > 0:
            self.__age = age

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, name):
        self.__name = name

shawn = PythonStudent1()
shawn.print_details()
shawn.age = 24
shawn.name = "Shawn"
shawn.print_details()