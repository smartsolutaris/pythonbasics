# Welcome to Python Basics

Here we will be introduced to the basic concepts in python
## Table of Content
* Print Function
* [Variables](#variables)
* [Numeric Types](#numeric-types)



## Variables
* Variables are containers for storing data. Think of variables as boxes where we can store something i.e. a value (implicitly).
* Python has no command for declaring a variable
* A variable is created the moment you first assign a value to it
* A variable:
   * has a name, 
   * has a value, 
   * has its place in the computer's memory.
* String variables can be declared either by using single or double quotes
* You can assign many values to multiple variables in one line
```python
    age = 10
    name = "john" 
    name = 'Alex'
```

### Naming Convention
* You can change the value of a variable by assigning a new value.
* The variable name can only contain letters: a-z, A-Z, numbers 0-9 and the _ symbol.
* It is case sensitive!
* The variable name cannot start with a digit!
* In Python, variables are named in the 
  * snake case style: each word is separated by an underscore (very_big_number) 
  * Pascal case style: a new word starts with a capital letter (VeryLongString)
  * Camel case style: each word, except the first doesnt start with a capital letter

```python
  total_number_of_students = 13 # snake case
  AccountInformation # pascal case, mostly used in Class definition
  totalAssignedStudeny = 10 # camelcase
```

## Print Function
To  output information to the users, print function is used `print()`
The function has the parameter in brackets - it is what we want to write and display on the screen.
```python
  print("Hello World")
  print("My name is Walley")
```

```python
first_string = "Python Basics Class"

print(first_string)
```

Create a variable `length`, assign a value and print the square of the length

## Comments
* Comments can be used to explain Python code.
* Comments can be used to make the code more readable.
* Comments can be used to prevent execution when testing code
* The # character starts the comment. Everything that follows it until the end of the line is ignored.
```python
    #My first variable
    std_age = 10
    
```
comments can be placed at the end of a line
```python
    std_age = 10 #the age of the first student
```
Python does not really have a syntax for multi line comments.
To add a multiline comment you could insert a # for each line
or use the multiline string

```python
# The student age in Class A
# who take course 102
# every mondays 10am
```
```python
    ```The student
    in a class of 4```
```

## Exercise One
* Create a variable `carname` and assign the value `Volvo` to it
* Create variables length and breath, assign values 10 and 12, print the area.
* Display the sum of 5 + 10, using two variables: x and y
* Is this a legal variable name `2my-first_name = "John"`

## Data Types
Several data types are built into Python:
* int - integers.
* float - floating-point numbers (real numbers).
* complex - complex numbers.
* str and bytes - text sequences.
* bool - boolean data type: true / false.
* NoneType - special, undefined type of non-existent values.

### Checking Data Types
In Python, we can check the data type by using the built-in `type()` function

## Numeric Types
* integers, floating point numbers, and complex numbers
* Variables of numeric types are created when you assign a value to them
* Int, or integer, is a whole number, positive or negative, without decimals, of unlimited length.
* Float, or "floating point number" is a number, positive or negative, containing one or more decimals.

- < 0 < +

```python

  first_num = 1 #int
  second_num = 3.12 # float
  complex_num = 1j # complex

print(type(complex_num))
```

Integer and Float support the following operations:

| Operator | Description|
|----------|-------------|
  |x+y| the sum of x+y|
|x-y| difference between x and y|
|x*y| product of x and y|
| x/y| division |
|x // y | floored division|
|x % y| remainder of x/y|
|-x| x negated|
|x ** y | x to the power y|

![img.png](img.png)

# Exercise Two
* Write a Python program to get the volume of a sphere with radius 6.
* Write a Python program which accepts the radius of a circle from the user and compute the area
* Write a Python program that accepts an integer (n) and computes the value of n+nn+nnn
```python
  #Example
  user_input = 6
  6 + 66 + 666
  returns 738
``` 
* Write a Python program to get the difference between a given number and 17, if the number is greater than 17 return double the absolute difference.

## String Data Types
string in python are surrounded by either single quote or double quote
Like many other popular programming languages, strings in Python are arrays of bytes representing unicode characters.
However, Python does not have a character data type, a single character is simply a string with a length of 1.
Square brackets can be used to access elements of the string.

* strings are arrays
* looping through strings
* Length of string
* 

### Formatting string
There are four ways to print data:
  * Without any formatting. 
  * The printf formatting from the C language - an older solution. 
  * The str.format() formatting - a newer solution. 
  * The f-string interpolation - the latest solution.

```python
# Displaying multiple strings with a separator simultaneously
    print("What", "a", "beautiful", "day", ".", sep="-")
    print("1", "2", 3, 4, 5, sep=" < ")
    fruit = "orange"
    print("apple", "banana", fruit, sep=" + ")

# Format and display (older solution)
    title = "General"
    name = "Kenobi"
    print("Hello there, %s %s" % (title, name))

# Format and display (newer solution)
    title = "General"
    name = "Kenobi"
    print("Hello there, {} {}".format(title, name))

 # Format and display (latest method)
    a = 2
    b = 7
    print(f"{a} times {b} to the power of 2 is {(a * b) ** 2}.")

# Changing the way the variable is displayed
    n = 109.2345654324
    print(f"{n: .3f}") # will display 109.234

    percent = 0.71
    print(f"{percent: .1%}") # will display 71.0%
```
# Casting
You can convert from one acceptable type to another
```python
 print(float(first_num))
```

# Exercise Three
* find the length of `intro = "Welcome to Python Basics"`
* Write a Python program which accepts the user's first and last name and print them in reverse order with a space between them
* Write a Python program to print the following string in a specific format (see the output). Go to the editor
Sample String : "Twinkle, twinkle, little star, How I wonder what you are! Up above the world so high, Like a diamond in the sky. Twinkle, twinkle, little star, How I wonder what you are" 
``` 
Twinkle, twinkle, little star,
	How I wonder what you are! 
		Up above the world so high,   		
		Like a diamond in the sky. 
Twinkle, twinkle, little star, 
	How I wonder what you are
```

