# if, else, multiple if (elif)

"""
    <<IF>> supports logical conditions => translates into True/False
    ==, !=, > , <, >=, <=, not, in
    Indentation is required after the condition expression
    statement/instructions is only executed when expression is True
    if <<condition expression>>:
        statements/instructions

    <<else>> runs when conditions are not met
    if <<condition expression>>:
        statements/instructions
    else:
        statements

    <<elif>>
    if <<condition expression>>:
        statements/instructions
    elif <<condition expression>>:
        statements/instructions
    .
    .
    .
    else:
        statements
"""
# check whether 7 is an even number
number = 11
if number % 2 == 0:
    print(f"{number} is an even number")
    square = number ** 2
    print(f"square of {number} is {square}")
else:
    print(f"{number} is an odd number")


print(f"{number + 10}")

num1, num2 = 100, 40

if num1 > num2:
    print(f"{num1} is greater than {num2}")
elif num1 < num2:
    print(f"{num1} is less than {num2}")
else:
    print(f"{num1} is equal to {num2}")

"""
    <<output>> if expression else <<output>>
"""

# single line if else
print("greater") if num1 > num2 else print("not_great")
"""
And => Only True and True returns true when using `and`
True and True = True
True and false = false
false and true = false
false and false = false

OR => only false or false returns false
"""

"""
   A+ = 95 and above
   A = btw 90 and 94
   B+ = btw 85 and 89
   b = 80 and 84
   B- = 70 and 79
   c+= = 65 and 69
   c = 60 and 64
   c- = 50 and 59
   F = <50
"""
score = 91
grade = None
if score >= 95:
    grade = 'A+'
elif (score >= 90) and (score < 95):
    grade = 'A'
elif (score >= 85) and (score < 89):
    pass
else:
    grade = 'F'

print(grade)